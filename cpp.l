%{

#include <stdio.h>
#include "y.tab.h"
int line_number = 1;

%}


%%

"void"		return(VOID);
"int"           return(INT);
"for"           return(FOR);
"do"            return(DO);
"while"         return(WHILE);
"if"            return(IF);
"else"          return(ELSE);
"main"	        return(MAIN);
"return"        return(RETURN);

[a-zA-Z]*       return(LETTER);

[0-9]+          return(DIGIT);
"_"             return(UNDERSCORE);
"="             return(ASSIGNMENT);
"=="            return(EQUAL);
"!="            return(NOTEQUAL);
">="            return(GEQ);
">"             return(GT);
"<="            return(LEQ);
"<"             return(LT);
"+"             return(PLUS);
"-"             return(MINUS);
"*"             return(STAR);
"("             return(LPAREN);
")"             return(RPAREN);
"{"             return(LBRACE);
"}"             return(RBRACE);
"&&"            return(AND);
"||"            return(OR);
"!"             return(NOT);
";"             return(SEMICOLON);
","             return(COMMA);

[\t\f " "]      ;

\n              line_number++;

%%


yywrap ()
{
  return 1;
}
